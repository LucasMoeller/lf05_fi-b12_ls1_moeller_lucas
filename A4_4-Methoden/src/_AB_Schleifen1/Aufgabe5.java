// LS1 A4.4
package _AB_Schleifen1;

public class Aufgabe5 {

	public static void main(String[] args) {
		
		for(int i = 1; i <= 10; i ++) {
			for(int j = 1; j <= 10; j++) {
				System.out.print(i*j + "\t");
			}
			System.out.println("");
		}

	}

}
