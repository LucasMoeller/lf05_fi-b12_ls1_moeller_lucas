package _AB_Schleifen1;

import java.util.Scanner;

public class Aufgabe1 {

	public static void main(String[] args) {
		Scanner kb = new Scanner(System.in);
		System.out.println("Geben Sie eine Grenze (int) ein:");
		int n = kb.nextInt();
		
		for(int i = 1; i <= n; i ++) {
			System.out.print(i +", ");
			
		}
		System.out.println("");
		for(int i = n; i >= 1; i --) {
			System.out.print(i +", ");
			
		}
	}

}
