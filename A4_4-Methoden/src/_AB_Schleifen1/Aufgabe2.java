// LS 1, A 4.4
package _AB_Schleifen1;

import java.util.Scanner;

public class Aufgabe2 {

	public static void main(String[] args) {
		
		Scanner kb = new Scanner(System.in);
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein: ");
		int grenze = kb.nextInt();
		
		int sumA = 0, 
			sumB = 0,
			sumC = 0;
		
		for(int a = 1; a <= grenze; a ++) {
			sumA += a;
		}
		
		for(int b = 1; b <= grenze; b ++) {
			sumB += 2*b;
		}
		
		for (int c = 0; c <= grenze; c++ ) {
			sumC += 2*c+1;
			
		}
		
		
		System.out.println("Die Summe f�r A betr�gt: "+sumA);
		System.out.println("Die Summe f�r B betr�gt: "+sumB);
		System.out.println("Die Summe f�r A betr�gt: "+sumC);
	}
}
