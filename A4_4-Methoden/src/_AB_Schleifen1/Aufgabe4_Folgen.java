// LS1 A4.4
package _AB_Schleifen1;

public class Aufgabe4_Folgen {

	public static void main(String[] args) {
		// a
		System.out.println("a)");
		for(int a = 99; a >= 9; a -= 3) {
			System.out.print(a + ", " );
		}
		
		// c
		System.out.println("\nc)");
		
		for(int c = 2; c <= 102; c += 4) {
			System.out.print(c + ", " );
		}
	
		// e
		System.out.println("\ne)");
		
		for(int e = 2; e <= 32768; e *= 2) {
			System.out.print(e + ", " );
		}
	
	}

}
