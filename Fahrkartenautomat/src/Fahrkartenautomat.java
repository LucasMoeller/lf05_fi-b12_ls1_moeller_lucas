﻿// Klasse für die Aufgaben
// A2.3
// A2.4
// A2.5

// A3.3

import java.util.ArrayList;
import java.util.Scanner;

class Fahrkartenautomat
{

	public static double fahrkartenbestellungErfassen () {
    	int anzahl = 0;
    	double einzelpreis;
    	
    	Scanner tastatur = new Scanner(System.in);
    
    // A 4.5  Ticketauswahl aus 3 Möglichkeiten 
    	ArrayList <String> tickets = new ArrayList<String>();
    	ArrayList <Double> preise = new ArrayList <Double>();
    	tickets.add("Einzelfahrschein Regeltarif AB");
    	preise.add(2.90);
    	tickets.add("Tageskarte Regeltarif AB");
    	preise.add(8.60);
    	tickets.add("Kleingruppen-Tageskarte Regeltarif AB");
    	preise.add(23.50);
    	
    	//System.out.print("Ticketpreis (EURO): ");
        //einzelpreis = tastatur.nextDouble();
       
        
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
    	for(int i = 0; i<3; i ++) {	
    		System.out.println("\t"+tickets.get(i) + " ["+preise.get(i)+"] ("+ (i+1) +")");
    	}
    	
    	System.out.print("\nIhre Wahl: ");
        int wahl = tastatur.nextInt();
        
        while(wahl < 1 || wahl > 3) {
        	System.out.println("Falsche Auswahl, bitte korrigiere!");
        	System.out.print("\nIhre Wahl: ");
        	wahl = tastatur.nextInt();
        }
        
        einzelpreis = preise.get(wahl-1);   // wahl-1, um den eigentlichen Index abzufragen.
    // --------------------------------------------------- A4.5
    	
        System.out.print("Anzahl Tickets, 1 bis 10 (Ganzzahl):");
   		anzahl = tastatur.nextInt();
   		System.out.println(anzahl + " sind gewünscht.\n");
    	while(anzahl < 1 || anzahl > 10) {
    		System.out.println("Ihre Eingabe war ungültig.\n>> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
    		System.out.print("Anzahl Tickets, 1 bis 10 (Ganzzahl):");
    		anzahl = tastatur.nextInt();
    	}
    	System.out.println("Die Anzahl " + anzahl+ " wurde gespeichert.");
   		
    	return anzahl * einzelpreis;
    	
    }
    
    public static double fahrkartenBezahlen (double zuZahlen) {
    	double eingezahlterGesamtbetrag = 0.0;
    	double zuZahlenderBetrag = zuZahlen;
    	double eingeworfeneMünze = 0.0;
    	
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;
        }
    	
    	double rueckgabe = eingezahlterGesamtbetrag - zuZahlenderBetrag; 
    	return rueckgabe;
    }
    
    public static void warte(int t) {
    	
    	for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
        	   Thread.sleep(t);
           } catch (InterruptedException e) {
        	   e.printStackTrace();
           }
        }
    	
    }
    public static void fahrkartenAusgeben (double rückgabebetrag) {
    	
    	
    	System.out.println("\nFahrschein wird ausgegeben");
       
    	warte(250);
       
    	System.out.println("\n\n");
        
       
        // Rückgeldausgabe
        rueckgeldAusgeben(rückgabebetrag);
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
        
    }
    
    public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
    }
    
    public static void rueckgeldAusgeben (double rückgabebetrag) {
    	
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.println("Der Rückgabebetrag in Höhe von " + rückgabebetrag + " EURO");
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
            	muenzeAusgeben(2, "EURO");
            	rückgabebetrag -= 2.0;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
            	muenzeAusgeben(1, "EURO");
            	rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
            	muenzeAusgeben(50, "CENT");
            	rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
            	muenzeAusgeben(20, "CENT");
  	          	rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
            	muenzeAusgeben(10, "CENT");
            	rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
            	muenzeAusgeben(5, "CENT");
            	rückgabebetrag -= 0.05;
            }
        }
    }

	
    public static void main(String[] args)
    {
       while(true) { 
    	   
    	   System.out.println("Fahrkartenbestellvorgang:\n=========================\n");
	       // Erfassung der Bestellung
	       double zuZahlenderBetrag = fahrkartenbestellungErfassen ();
	       
	       // Rückgeldberechnung mit Geldeinwurf
	       // ------------------------------- 
	       double rückgabebetrag = fahrkartenBezahlen (zuZahlenderBetrag);
	       
	       // Fahrscheinausgabe
	       // -----------------
	       fahrkartenAusgeben (rückgabebetrag);
	       
	       // Rückgeldausgabe
	       // -----------------
	       // Die Methode rueckgeldAusgeben() wird in Zeile 69 von fahrkartenAusgeben() aufgerufen, noch
	       // bevor der Abschiedsgruß augegeben wird.
	       
	       System.out.println("\n\n\n\n");
       } 
       
    }
}

/* ------------------- Antworten zur Aufgabe A2.5 -----------------------------------------

Schritt 1.
	double zuZahlenderBetrag; 
    double eingezahlterGesamtbetrag;
    double eingeworfeneMünze;
    double rückgabebetrag;



Schritt 2. 
 	=	Zuweisung
 	
 	+	Addition
 	*	Multiplikation
 	-	Subtraktion

	+=  Inkrement
	++  Inkrement um 1
	-= 	Dekrement
	
	Vergleiche	
	 	>	größer als ...
	 	<	kleiner als ...
	 	>=	größer oder gleich
 		

*/
// 5. Ich habe für die Anzahl einen Integer-Wert gewählt, weil es nur ganze Tickets gegen kann.

// 6. Der Ausdruck anzahl * einzelpreis bestimmt nun den zu zahlenden Betrag, 
//    abhängig von der Anzahl an gewünschten Tickets und den Einzelpreis.