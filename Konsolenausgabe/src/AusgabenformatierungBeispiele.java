public class AusgabenformatierungBeispiele{
	
	
	public static void main(String[] args) {
		
		//Integer
		//System.out.printf("|%+-10d%+10d|", 50, -50);
		//Float
		//System.out.printf("|%+-10.2f|", 2.25);
		//String
		//System.out.printf("|%-10.4s|", "Lucas");   // out: " |Luca      |
		System.out.printf("Name:%-10sAlter:%-8dGewicht:%-10.2f", "Max", 18, 80.50);
		
	}
	
}