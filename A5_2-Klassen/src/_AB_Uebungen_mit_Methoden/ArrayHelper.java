// A 5.2
package _AB_Uebungen_mit_Methoden;

public class ArrayHelper {
	// zu Aufgabe 1:
	public static String convertArrayToString(int[] zahlen) {
		String ergebnis = "";
		for(int i = 0; i < zahlen.length; i ++) {
			
			// Stringkonkatenation
			ergebnis += zahlen[i];
			ergebnis += ", ";
		}
		return ergebnis;
	}
	
	// zu Aufgabe 2
	public static int[] reverseArray(int[] eingabe) {
		
		int ablage;
		for(int i = 0; i < eingabe.length / 2; i ++) {
			ablage = eingabe[i];
			eingabe[i] = eingabe[eingabe.length -1-i];
			eingabe[eingabe.length -1-i] = ablage;
			
		}
		return eingabe;
	}
	
	// zu Aufgabe 3
	public static int[] umkehrenMitZweitArr(int[] eingabe) {
		int[] ergebnis = new int[eingabe.length];
		
		for (int i = 0; i < ergebnis.length; i++ ) {
			
			
			ergebnis[i] = eingabe[eingabe.length-1-i];
			
		}
		
		return ergebnis;
	}
	// zu Aufgabe 4 - Temperaturtabelle
	public static double[][] temperaturen(int zeilen){
		
		double[][] wertepaare = new double[zeilen][2];
		double wertF = 0.0;
		for( int z = 0; z < zeilen; z ++) {
			
			wertepaare[z][0] = wertF;
			wertepaare[z][1] = (5.0/9) * (wertF - 32);
			System.out.println(wertF + "\t|\t" + wertepaare[z][1]);
			
			wertF += 10.0;
		}
		return wertepaare;
	}
	
	// zu Aufgabe 5 - n x m -Matrix
	public static int[][] createM(int m, int n){
		
		return new int[m][n];
	}
	
	public static void writeValue(int[][] matrix, int m, int n, int value) {
		
		matrix[m][n] = value;
	}
	
	public static void main(String[] args) {
		
		int[] zahlen = {1,2,3,4,5,6};
		System.out.println("zu Aufgabe 1, als String:\t" + convertArrayToString(zahlen));
		System.out.print("zu Aufgabe 2, umgekehrt:\t");
		int[] ergebnisAufgabe2 = reverseArray(zahlen); 
		
		for(int i = 0; i < ergebnisAufgabe2.length; i ++ ) {
			System.out.print(ergebnisAufgabe2[i] + ", ");
		}

		// zu Aufgabe 3:
		int[] zahlen3 = {10,20,30,40,50,60};
		System.out.println("\n\nzu Aufgabe 3, das Array:\n"
			+ convertArrayToString(zahlen3) 
			+"\numgekehrt:\n" 
			+ convertArrayToString(umkehrenMitZweitArr(zahlen3))
		);
		
		// zu Aufgabe 4:
		System.out.println("\nzu Aufgabe 4:\nTabelle von Temperaturen in �F und �C");
		System.out.println("�F \t|\t �C");
		double[][] temperaturTabelle = temperaturen(5);
		
		
		// zu Aufgabe 5:
		System.out.println("\nzu Aufgabe 5:");
		int[][] matrix = createM(5,2);
		for(int spalte = 0; spalte < 5; spalte ++) {
			
			writeValue(matrix, spalte, 0, spalte);
			writeValue(matrix, spalte, 1, spalte * 100);
			System.out.println(matrix[spalte][0] + "\t|\t" + matrix[spalte][1]);
		}
		
	}
	
	

}
