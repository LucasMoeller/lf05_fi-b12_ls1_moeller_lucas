// A 5.2
package _AB_Einfache_Uebungen;

public class Aufgabe2_UngeradeZahlen {

	public static void main(String[] args) {
		
		int[] ungeradeZahlen = new int[10];
		int stelle = 0;
		for(int j = 1; j < 20; j ++) {	
			
			// Wenn die Ganzzahldivision nicht der vollständigen Division gleicht:
			if( j /2  != (double) j / 2) {  	
			/*
				5 / 2	!=	5.0 / 2
				2		!= 	2.5
				
				-> 5 ist ungerade.
			 */
				
				if(stelle < 10) {		// verhindert Index-Überlauf, Index endet bei 9.
					ungeradeZahlen[stelle] = j;	
				}
				stelle ++;
			}
		}
	
		for(int n = 0; n < 10; n ++) {
			System.out.print(ungeradeZahlen[n]+ ",");
		}
	}

}
