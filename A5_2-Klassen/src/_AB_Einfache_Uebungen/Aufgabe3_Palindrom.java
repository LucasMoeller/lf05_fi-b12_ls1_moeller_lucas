// A 5.2
package _AB_Einfache_Uebungen;

import java.util.Scanner;

public class Aufgabe3_Palindrom {

	public static void main(String[] args) {
		char[] wort = new char[5];
		
		Scanner tast = new Scanner(System.in);
				
		for(int i = 0; i < 5; i ++) {
			System.out.print("Geben Sie ein Zeichen ein: ");
			wort[i] = tast.nextLine().charAt(0);
			
		}
		for(int i = 4; i >= 0; i --) {
			System.out.print(wort[i] + ", ");
		}
	}

}
