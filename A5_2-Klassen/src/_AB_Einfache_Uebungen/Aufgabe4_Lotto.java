// A 5.2
package _AB_Einfache_Uebungen;

public class Aufgabe4_Lotto {

	public static void main(String[] args) {
		
		int[] lottoZahlen = {3, 7, 12, 18, 37, 42};
		
		//  a)
		System.out.print("[ ");
		for(int i=0; i < 6; i ++) {
			System.out.printf(" %d ", lottoZahlen[i]);
		}
		System.out.print(" ] \n");
		
		// b)
		for(int i = 0; i < 6; i ++) {
			if(lottoZahlen[i] == 12)
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
			
			if(lottoZahlen[i] == 13)
				System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
		}
		
	}

}
