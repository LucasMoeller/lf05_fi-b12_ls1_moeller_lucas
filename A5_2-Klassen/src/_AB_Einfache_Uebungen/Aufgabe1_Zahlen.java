// A 5.2
package _AB_Einfache_Uebungen;

public class Aufgabe1_Zahlen {

	public static void main(String[] args) {
		
		int[] zahlenreihe = new int[10];
		
		System.out.println("Zahlenreihe: ");
		for(int i = 0; i < 10; i ++) {
			zahlenreihe[i] = i;
			System.out.print(zahlenreihe[i] + ", ");
			
		}
		
	}

}
