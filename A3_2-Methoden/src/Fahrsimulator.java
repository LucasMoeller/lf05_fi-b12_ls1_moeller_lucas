// LS 05.1 A3.2, AB-MethodenProgrammierübungen-3.pdf
// Aufgabe 6

import java.util.Scanner;

public class Fahrsimulator {

	public static double beschleunige(double v,double dv) {
		
		return v + dv;
	}
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		double v = 0.0;
		double dv;
		
		boolean runs = true;
		while(runs) {
			
			System.out.println("Die Geschwindigkeit in km/h beträgt " +v);
			System.out.println("Geben Sie einen Wert für die Beschleunigung ein:");
			dv = sc.nextDouble();
			v = beschleunige(v,dv);
			
			if(v+dv > 130 ) {
				v = 130;
	
			} 
			else if(v + dv < 0) {
				v = 0;
			}
			
			System.out.println("Die neue Geschwindigkeit in km/h beträgt " +v + "\n\n");
		}
		
	}

}
