
// AB-MethodenProgrammierübungen-2.pdf, Aufgabe 2   PCHaendler.java:


import java.util.Scanner;

public class PCHaendler {
	
	
	public static String liesString(String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		String str = myScanner.next();
		return str;
	}
	
	public static int liesInt(String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		int integer = myScanner.nextInt();
		return integer;
	}
	
	public static double liesDouble(String text){
		Scanner myScanner = new Scanner(System.in);
		System.out.println(text);
		double doub = myScanner.nextDouble();
		return doub;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis){
		return anzahl * nettopreis;
	}
	
	public static double berechneGesamtnettopreis(double nettogesamtpreis, double mwst){
		return ( nettogesamtpreis * (1 + mwst / 100));
	}

	public static void
	rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis,double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

	
	
	public static void main(String[] args) {
		
		

		// Benutzereingaben lesen
		
		String artikel = liesString("was moechten Sie bestellen?");
		
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		
		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl , preis);
		double bruttogesamtpreis = berechneGesamtnettopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechungausgeben(artikel,  anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

	}

}