import java.util.Scanner;
public class Addition {

	public static int getNum(String bezeichnung) {
	
		Scanner sc = new Scanner(System.in);
		System.out.println(bezeichnung + " Zahl eingeben:");
		
		return sc.nextInt();		
		// Der R�ckgabewert von sc.nextInt() des Typs int wird direkt weitergegeben.
	}
	
	public static int addiere(int a, int b) {
		
		return a + b;
	}

	public static void main(String[] args) {


		int num1 = getNum("Erste"); 
		int num2 = getNum("Zweite"); 
		System.out.println( "Summe:\n" + addiere( num1, num2 ) + "\n" );

		// Zweiter Aufruf von addiere, diesmal ohne Deklaration von Variablen:
		System.out.println( "\nSumme:\n" + addiere( getNum("Dritte"), getNum("Vierte") )  );
		
	}

}
