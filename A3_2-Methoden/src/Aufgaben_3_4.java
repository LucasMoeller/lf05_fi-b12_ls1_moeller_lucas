// LS 05.1 A3.2, AB-MethodenProgrammierübungen-3.pdf
// Aufgaben 3,4

public class Aufgaben_3_4 {

	// Aufgabe 3:
	public static double reihenschaltung(double r1,double r2) {
		
		//Ersatzwiderstand, als Summe beider Parameter:
		return r1 + r2;
		
	}
	// Aufgabe 4:
	
	public static double parallelschaltung(double r1,double r2) {
		
		// Formel aus Infomaterial: IB-Methoden, Funktionen.pdf
		return 1.0 / (1.0 / r1 + 1.0 / r2);
	}
	
	
	
	public static void main(String[] args) {
		
		System.out.println(reihenschaltung(2.5, 5.0));
		System.out.println(parallelschaltung(3.0, 7.0));
	}

}
