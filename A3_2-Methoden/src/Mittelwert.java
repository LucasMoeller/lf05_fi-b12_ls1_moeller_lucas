// AB-MethodenProgrammierübungen-2.pdf, Aufgabe 1:

public class Mittelwert {

	public static double berechneMittelwert(double a, double b) {

		return ((a + b) / 2.0);
	}

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================
		double x = 2.0;
		double y = 4.0;
		double m;

		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		m = (x + y) / 2.0;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
		
		
		
		// Meine neue Ausgabe:
		double a = 5.0;
		double b = 6.4;
		System.out.printf(" \n \nDer Mittelwert von %.2f und %.2f ist %.2f", a, b,  berechneMittelwert(a,b));
	}
}
