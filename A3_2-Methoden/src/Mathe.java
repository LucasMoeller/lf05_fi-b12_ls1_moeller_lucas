// LS 05.1 A3.2, AB-MethodenProgrammierübungen-3.pdf




public class Mathe {

	// Aufgabe 5
	public static double quadrat(double x) {
		return x * x;
	}
	// Aufgabe 7
	public static double hypotenuse(double kathete1, double kathete2) {
		
		return Math.sqrt( quadrat(kathete1)  + quadrat(kathete2));
	}
	
	public static void main(String[] args) {
		
		// zu Aufgabe 5
		System.out.println(quadrat(12.0)); // -> 144.0
	
		// zu Aufgabe 7
		System.out.println(hypotenuse(3.0, 4.0)); // -> 5.0
	}

}
